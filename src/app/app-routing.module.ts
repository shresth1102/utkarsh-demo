import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SearchMovieComponent} from './search-movie/search-movie.component';
import {AddMovieComponent} from './add-movie/add-movie.component';


const routes: Routes = [
  {path:'addMovie',component:AddMovieComponent},
  {path:'searchMovie',component:SearchMovieComponent},  
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
