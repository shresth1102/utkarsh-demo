import { Component, OnInit } from '@angular/core';

import { MovieServiceService } from '../services/movie-service.service';
import {MovieModel} from '../movie/movie-model';

@Component({
  selector: 'app-search-movie',
  templateUrl: './search-movie.component.html',
  styleUrls: ['./search-movie.component.css']
})
export class SearchMovieComponent implements OnInit {

  submitted: boolean;
  notFound: boolean;
  movieCategory: string;
  movieList: any = [];
  searchData: MovieModel[];
  constructor(private myService: MovieServiceService) {
   /* this.myService.getMovieList().subscribe(
      list => {
        this.movieList = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });

      });*/
     // this.searchData=[];
  }

  ngOnInit(): void {
    this.movieList = [
      { id: 1, name: 'JhonWick', rating: 5 },
      { id: 1, name: 'F&F', rating: 4 },
    ];
   
  }

  searchMovie() {
   
    this.submitted = true;

   // this.searchData = this.movieList.filter(data => data.movieCategory === this.movieCategory);
   console.log(this.movieCategory);
   
    this.myService.getByGenre(this.movieCategory).subscribe(
      (data) => {
        this.searchData = data;
      }

     
    );

    if (this.searchData.length<=0) {
      this.submitted = false;
      this.notFound = true;
      setTimeout(() => this.notFound = false, 3000);
    }
    console.log(this.searchData);

  }
}
