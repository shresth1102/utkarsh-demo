import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { SearchMovieComponent } from './search-movie/search-movie.component';
import { AddMovieComponent } from './add-movie/add-movie.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {AngularFireModule} from '@angular/fire';
// import {AngularFirestoreModule} from '@angular/fire/firestore';
// import {AngularFireDatabaseModule} from '@angular/fire/database';
import {environment} from '../environments/environment'
import { HttpClientModule } from '@angular/common/http';

import { MovieServiceService } from './services/movie-service.service'

@NgModule({
  declarations: [
    AppComponent,
    SearchMovieComponent,
    AddMovieComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    // AngularFireModule.initializeApp(environment.firebase),
    // AngularFireDatabaseModule,
    // AngularFirestoreModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [MovieServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
