import { Component, OnInit } from '@angular/core';
import { MovieServiceService} from '../services/movie-service.service';
import {MovieModel} from '../movie/movie-model';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  movie:MovieModel;
  constructor(public movService:MovieServiceService) {
    this.movie=new MovieModel();
   }

  submitted:boolean=false;
  success:boolean=false;
  formControls=this.movService.myForm.controls;


  ngOnInit(): void {
  }

  onSubmit(){
    this.submitted=true;

    if(this.movService.myForm.valid){

     // this.movService.addMovie(this.movService.myForm.value);
     this.movie=Object.assign(this.movie,this.movService.myForm.value);
     console.log(this.movie);
      let obr=this.movService.add(this.movie);

      obr.subscribe(
        (data) => {
          //this.router.navigateByUrl("/contacts");
          this.success=true
          setTimeout(()=>this.success=false,3000);
        }
      );

      
     
      this.submitted=false;
      this.movService.myForm.reset();

    }

  }
}
