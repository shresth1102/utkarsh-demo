import { Injectable } from '@angular/core';
//import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
// import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {MovieModel} from '../movie/movie-model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MovieServiceService {

  baseUrl:string
  constructor(private http:HttpClient) {
   // this.movieList = this.afd.list("Movies");
    this.baseUrl=`http://localhost:4200/movies`;
  }
 // movieList: AngularFireList<any>;

  myForm = new FormGroup(
    {
      moviesId: new FormControl(0),
      moviesName: new FormControl('', [Validators.required, Validators.pattern("[a-zA-Z ]*")]),
      moviesRating: new FormControl('', [Validators.required, Validators.pattern("[1-9]")]),
      moviesGenre: new FormControl('', Validators.required),
    }
  );

  

 /* addMovie(movie) {
    this.movieList.push({
      movieId: movie.movieId,
      movieName: movie.movieName,
      movieRating: movie.movieRating,
      movieCategory: movie.movieCategory
    });
  }
  getMovieList() {
    return this.movieList.snapshotChanges();
  }*/

  add(movie: MovieModel): Observable<MovieModel> {
    return this.http.post<MovieModel>(this.baseUrl, movie);
  }
  getByGenre(genre: string): Observable<MovieModel[]> {
    return this.http.get<MovieModel[]>(`${this.baseUrl}/moviesgenre/${genre}`);
  }
}
