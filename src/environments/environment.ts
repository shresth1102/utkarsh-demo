// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBKSkTGuLhEr6wu72LpwvMIyzRx_IJNiOE",
    authDomain: "angularapp-640b6.firebaseapp.com",
    databaseURL: "https://angularapp-640b6.firebaseio.com",
    projectId: "angularapp-640b6",
    storageBucket: "angularapp-640b6.appspot.com",
    messagingSenderId: "746868258627",
    appId: "1:746868258627:web:0938462badf731a654e1f6",
    measurementId: "G-T6WH517G30"
  },
  baseMwUrl:'http://localhost:7777/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
